.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mconv_buffer_converter" 3m17n "Mon Sep 25 2023" "Version 1.8.4" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mconv_buffer_converter \- 


.SH SYNOPSIS
\fBMConverter\fP*
\fBmconv_buffer_converter\fP (MSymbol
\fIname\fP, const unsigned char *
\fIbuf\fP, int
\fIn\fP)


.SH DESCRIPTION
.PP
Create a code converter bound to a buffer\&. The
.ft B
mconv_buffer_converter()
.ft R
function creates a pointer to a code converter for coding system
.ft B
name\fP\&. The code converter is bound to buffer area of
.ft B
n
.ft R
bytes pointed to by
.ft B
buf\fP\&. Subsequent decodings and encodings are done to/from this buffer area\&.
.PP
\fBname
.ft R
can be
.ft B
Mnil\fP\&. In this case, a coding system associated with the current locale (LC_CTYPE) is used\&.
.PP
.SH RETURN VALUE
.PP
.RS 4
If the operation was successful,
.ft B
mconv_buffer_converter()
.ft R
returns the created code converter\&. Otherwise it returns
.ft C
NULL
.ft R
and assigns an error code to the external variable
.ft B
merror_code\fP\&. 
.br
.RE
.PP
.SH ERRORS
.PP
.RS 4
\fCMERROR_SYMBOL\fP,
.ft R
.ft C
MERROR_CODING
.ft R
.RE
.PP
\fBSee Also:
.ft R

.RS 4
\fBmconv_stream_converter()
.ft R
.br
 
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
