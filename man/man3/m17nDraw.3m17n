.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "m17nDraw" 3m17n "Mon Sep 25 2023" "Version 1.8.4" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
m17nDraw_\-_Dra \-  \- Drawing M\-texts on a window\&.  

.SH SYNOPSIS
.br
.PP
.SS "Data Structures"

.in +1c
.ti -1c
.RI "struct \fBMDrawControl\fP"
.br
.RI "Type of a text drawing control\&. "
.ti -1c
.RI "struct \fBMDrawMetric\fP"
.br
.RI "Type of metric for glyphs and texts\&. "
.ti -1c
.RI "struct \fBMDrawGlyphInfo\fP"
.br
.RI "Type of information about a glyph\&. "
.ti -1c
.RI "struct \fBMDrawGlyph\fP"
.br
.RI "Type of information about a glyph metric and font\&. "
.in -1c
.SS "Typedefs"

.in +1c
.ti -1c
.RI "typedef void * \fBMDrawWindow\fP"
.br
.RI "Window system dependent type for a window\&. "
.ti -1c
.RI "typedef void * \fBMDrawRegion\fP"
.br
.RI "Window system dependent type for a region\&. "
.in -1c
.SS "Functions"

.in +1c
.ti -1c
.RI "int \fBmdraw_text\fP (\fBMFrame\fP *frame, \fBMDrawWindow\fP win, int x, int y, \fBMText\fP *mt, int from, int to)"
.br
.RI "Draw an M-text on a window\&. "
.ti -1c
.RI "int \fBmdraw_image_text\fP (\fBMFrame\fP *frame, \fBMDrawWindow\fP win, int x, int y, \fBMText\fP *mt, int from, int to)"
.br
.RI "Draw an M-text on a window as an image\&. "
.ti -1c
.RI "int \fBmdraw_text_with_control\fP (\fBMFrame\fP *frame, \fBMDrawWindow\fP win, int x, int y, \fBMText\fP *mt, int from, int to, \fBMDrawControl\fP *control)"
.br
.RI "Draw an M-text on a window with fine control\&. "
.ti -1c
.RI "int \fBmdraw_text_extents\fP (\fBMFrame\fP *frame, \fBMText\fP *mt, int from, int to, \fBMDrawControl\fP *control, \fBMDrawMetric\fP *overall_ink_return, \fBMDrawMetric\fP *overall_logical_return, \fBMDrawMetric\fP *overall_line_return)"
.br
.RI "Compute text pixel width\&. "
.ti -1c
.RI "int \fBmdraw_text_per_char_extents\fP (\fBMFrame\fP *frame, \fBMText\fP *mt, int from, int to, \fBMDrawControl\fP *control, \fBMDrawMetric\fP *ink_array_return, \fBMDrawMetric\fP *logical_array_return, int array_size, int *num_chars_return, \fBMDrawMetric\fP *overall_ink_return, \fBMDrawMetric\fP *overall_logical_return)"
.br
.RI "Compute the text dimensions of each character of M-text\&. "
.ti -1c
.RI "int \fBmdraw_coordinates_position\fP (\fBMFrame\fP *frame, \fBMText\fP *mt, int from, int to, int x_offset, int y_offset, \fBMDrawControl\fP *control)"
.br
.RI "Return the character position nearest to the coordinates\&. "
.ti -1c
.RI "int \fBmdraw_glyph_info\fP (\fBMFrame\fP *frame, \fBMText\fP *mt, int from, int pos, \fBMDrawControl\fP *control, \fBMDrawGlyphInfo\fP *info)"
.br
.RI "Compute information about a glyph\&. "
.ti -1c
.RI "int \fBmdraw_glyph_list\fP (\fBMFrame\fP *frame, \fBMText\fP *mt, int from, int to, \fBMDrawControl\fP *control, \fBMDrawGlyph\fP *glyphs, int array_size, int *num_glyphs_return)"
.br
.RI "Compute information about glyph sequence\&. "
.ti -1c
.RI "void \fBmdraw_text_items\fP (\fBMFrame\fP *frame, \fBMDrawWindow\fP win, int x, int y, \fBMDrawTextItem\fP *items, int nitems)"
.br
.RI "Draw one or more textitems\&. "
.ti -1c
.RI "int \fBmdraw_default_line_break\fP (\fBMText\fP *mt, int pos, int from, int to, int line, int y)"
.br
.RI "Calculate a line breaking position\&. "
.ti -1c
.RI "void \fBmdraw_per_char_extents\fP (\fBMFrame\fP *frame, \fBMText\fP *mt, \fBMDrawMetric\fP *array_return, \fBMDrawMetric\fP *overall_return)"
.br
.RI "Obtain per character dimension information\&. "
.ti -1c
.RI "void \fBmdraw_clear_cache\fP (\fBMText\fP *mt)"
.br
.RI "clear cached information\&. 
.br
 "
.in -1c
.SS "Variables"

.in +1c
.ti -1c
.RI "int \fBmdraw_line_break_option\fP"
.br
.RI "Option of line breaking for drawing text\&. "
.in -1c
.SH "Detailed Description"
.PP 
Drawing M\-texts on a window\&. 

The m17n GUI API provides functions to draw M\-texts\&.
.PP
The fonts used for drawing are selected automatically based on the fontset and the properties of a face\&. A face also specifies the appearance of M\-texts, i\&.e\&. font size, color, underline, etc\&.
.PP
The drawing format of M\-texts can be controlled in a variety of ways, which provides powerful 2\-dimensional layout facility\&. 
.br
.PP

.br
 
.SH "Data Structure Documentation"
.SS MDrawControl
.PP
Type of a text drawing control\&.  

.ad l
.nh
.PP
\fBFIELD DOCUMENTATION:\fP
.PP 
.PP
\fBunsigned MDrawControl::as_image\fP
If nonzero, draw an M\-text as image, i\&.e\&. with background filled with background colors of faces put on the M\-text\&. Otherwise, the background is not changed\&. 
.br
 
.PP
\fBunsigned MDrawControl::align_head\fP
If nonzero and the first glyph of each line has negative lbearing, shift glyphs horizontally to right so that no pixel is drawn to the left of the specified position\&. 
.br
 
.PP
\fBunsigned MDrawControl::two_dimensional\fP
If nonzero, draw an M\-text two\-dimensionally, i\&.e\&., newlines in M\-text breaks lines and the following characters are drawn in the next line\&. If <format> is non\-NULL, and the function returns nonzero line width, a line longer than that width is also broken\&. 
.br
 
.PP
\fBunsigned MDrawControl::orientation_reversed\fP
If nonzero, draw an M\-text to the right of a specified position\&. 
.br
 
.PP
\fBunsigned MDrawControl::enable_bidi\fP
If nonzero, reorder glyphs correctly for bidi text\&. 
.PP
\fBunsigned MDrawControl::ignore_formatting_char\fP
If nonzero, don't draw characters whose general category (in Unicode) is Cf (Other, format)\&. 
.br
 
.PP
\fBunsigned MDrawControl::fixed_width\fP
If nonzero, draw glyphs suitable for a terminal\&. Not yet implemented\&. 
.br
 
.PP
\fBunsigned MDrawControl::anti_alias\fP
If nonzero, draw glyphs with anti\-aliasing if a backend font driver supports it\&. 
.br
 
.PP
\fBunsigned MDrawControl::disable_overlapping_adjustment\fP
If nonzero, disable the adjustment of glyph positions to avoid horizontal overlapping at font boundary\&. 
.br
 
.PP
\fBunsigned int MDrawControl::min_line_ascent\fP
If nonzero, the values are minimum line ascent pixels\&. 
.PP
\fBunsigned int MDrawControl::min_line_descent\fP
If nonzero, the values are minimum line descent pixels\&. 
.PP
\fBunsigned int MDrawControl::max_line_ascent\fP
If nonzero, the values are maximum line ascent pixels\&. 
.PP
\fBunsigned int MDrawControl::max_line_descent\fP
If nonzero, the values are maximum line descent pixels\&. 
.PP
\fBunsigned int MDrawControl::max_line_width\fP
If nonzero, the value specifies how many pixels each line can occupy on the display\&. The value zero means that there is no limit\&. It is ignored if <format> is non\-NULL\&. 
.br
 
.PP
\fBunsigned int MDrawControl::tab_width\fP
If nonzero, the value specifies the distance between tab stops in columns (the width of one column is the width of a space in the default font of the frame)\&. The value zero means
.IP "8." 4

.br
 
.PP

.PP
\fBvoid(* MDrawControl::format) (int line, int y, int *indent, int *width)\fP
If non\-NULL, the value is a function that calculates the indentation and width limit of each line based on the line number LINE and the coordinate Y\&. The function store the indentation and width limit at the place pointed by INDENT and WIDTH respectively\&.
.PP
The indentation specifies how many pixels the first glyph of each line is shifted to the right (if the member <orientation_reversed> is zero) or to the left (otherwise)\&. If the value is negative, each line is shifted to the reverse direction\&.
.PP
The width limit specifies how many pixels each line can occupy on the display\&. The value 0 means that there is no limit\&.
.PP
LINE and Y are reset to 0 when a line is broken by a newline character, and incremented each time when a long line is broken because of the width limit\&.
.PP
This has an effect only when <two_dimensional> is nonzero\&. 
.br
 
.PP
\fBint(* MDrawControl::line_break) (\fBMText\fP *mt, int pos, int from, int to, int line, int y)\fP
If non\-NULL, the value is a function that calculates a line breaking position when a line is too long to fit within the width limit\&. POS is the position of the character next to the last one that fits within the limit\&. FROM is the position of the first character of the line, and TO is the position of the last character displayed on the line if there were not width limit\&. LINE and Y are the same as the arguments to <format>\&.
.PP
The function must return a character position to break the line\&.
.PP
The function should not modify MT\&.
.PP
The \fBmdraw_default_line_break()\fP function is useful for such a script that uses SPACE as a word separator\&. 
.br
 
.PP
\fBint MDrawControl::with_cursor\fP
If nonzero, show the cursor according to <cursor_width>\&. 
.PP
\fBint MDrawControl::cursor_pos\fP
Specifies the character position to display a cursor\&. If it is greater than the maximum character position, the cursor is displayed next to the last character of an M\-text\&. If the value is negative, even if <cursor_width> is nonzero, cursor is not displayed\&. 
.br
 
.PP
\fBint MDrawControl::cursor_width\fP
If nonzero, display a cursor at the character position <cursor_pos>\&. If the value is positive, it is the pixel width of the cursor\&. If the value is negative, the cursor width is the same as the underlining glyph(s)\&. 
.br
 
.PP
\fBint MDrawControl::cursor_bidi\fP
If nonzero and <cursor_width> is also nonzero, display double bar cursors; at the character position <cursor_pos> and at the logically previous character\&. Both cursors have one pixel width with horizontal fringes at upper or lower positions\&. 
.br
 
.PP
\fBint MDrawControl::partial_update\fP
If nonzero, on drawing partial text, pixels of surrounding texts that intrude into the drawing area are also drawn\&. For instance, some CVC sequence of Thai text (C is consonant, V is upper vowel) is drawn so that V is placed over the middle of two Cs\&. If this CVC sequence is already drawn and only the last C is drawn again (for instance by updating cursor position), the right half of V is erased if this member is zero\&. By setting this member to nonzero, even with such a drawing, we can keep this CVC sequence correctly displayed\&. 
.br
 
.PP
\fBint MDrawControl::disable_caching\fP
If nonzero, don't cache the result of any drawing information of an M\-text\&. 
.br
 
.PP
\fB\fBMDrawRegion\fP MDrawControl::clip_region\fP
If non\-NULL, limit the drawing effect to the specified region\&. 

.SS MDrawMetric
.PP
Type of metric for glyphs and texts\&.  

.ad l
.nh
.PP
\fBFIELD DOCUMENTATION:\fP
.PP 
.PP
\fBint MDrawMetric::x\fP
X coordinates of a glyph or a text\&. 
.PP
\fBint MDrawMetric::y\fP
Y coordinates of a glyph or a text\&. 
.PP
\fBunsigned int MDrawMetric::width\fP
Pixel width of a glyph or a text\&. 
.PP
\fBunsigned int MDrawMetric::height\fP
Pixel height of a glyph or a text\&. 

.SS MDrawGlyphInfo
.PP
Type of information about a glyph\&.  

.ad l
.nh
.PP
\fBFIELD DOCUMENTATION:\fP
.PP 
.PP
\fBint MDrawGlyphInfo::from\fP
Start position of character range corresponding to the glyph\&. 
.PP
\fBint MDrawGlyphInfo::to\fP
End position of character range corresponding to the glyph\&. 
.PP
\fBint MDrawGlyphInfo::line_from\fP
Start position of character range corresponding to the line of the glyph\&. 
.PP
\fBint MDrawGlyphInfo::line_to\fP
End position of character range corresponding to the line of the glyph\&. 
.PP
\fBint MDrawGlyphInfo::x\fP
X coordinates of the glyph\&. 
.PP
\fBint MDrawGlyphInfo::y\fP
Y coordinates of the glyph\&. 
.PP
\fB\fBMDrawMetric\fP MDrawGlyphInfo::metrics\fP
Metric of the glyph\&. 
.PP
\fB\fBMFont\fP* MDrawGlyphInfo::font\fP
Font used for the glyph\&. Set to NULL if no font is found for the glyph\&. 
.br
 
.PP
\fBint MDrawGlyphInfo::prev_from\fP
Character ranges corresponding to logically previous glyphs\&. Note that we do not need the members prev_to because it must be the same as the member <from>\&. 
.br
 
.PP
\fBint MDrawGlyphInfo::next_to\fP
Character ranges corresponding to logically next glyphs\&. Note that we do not need the members next_from because it must be the same as the member <to> respectively\&. 
.br
 
.PP
\fBint MDrawGlyphInfo::left_from\fP
Start position of character ranges corresponding to visually left glyphs\&. 
.br
 
.PP
\fBint MDrawGlyphInfo::left_to\fP
End position of character ranges corresponding to visually left glyphs\&. 
.br
 
.PP
\fBint MDrawGlyphInfo::right_from\fP
Start position of character ranges corresponding to visually right glyphs\&. 
.br
 
.PP
\fBint MDrawGlyphInfo::right_to\fP
End position of character ranges corresponding to visually left glyphs\&. 
.br
 
.PP
\fBint MDrawGlyphInfo::logical_width\fP
Logical width of the glyph\&. Nominal distance to the next glyph\&. 
.br
 

.SS MDrawGlyph
.PP
Type of information about a glyph metric and font\&.  

.ad l
.nh
.PP
\fBFIELD DOCUMENTATION:\fP
.PP 
.PP
\fBint MDrawGlyph::from\fP
Character range corresponding to the glyph\&. 
.PP
\fBint MDrawGlyph::to\fP

.PP
\fBint MDrawGlyph::glyph_code\fP
Font glyph code of the glyph\&. 
.PP
\fBint MDrawGlyph::x_advance\fP
Logical width of the glyph\&. Nominal distance to the next glyph\&. 
.br
 
.PP
\fBint MDrawGlyph::y_advance\fP
Logical height of the glyph\&. Nominal distance to the next glyph\&. 
.br
 
.PP
\fBint MDrawGlyph::x_off\fP
X offset relative to the glyph position\&. 
.PP
\fBint MDrawGlyph::y_off\fP
Y offset relative to the glyph position\&. 
.PP
\fBint MDrawGlyph::lbearing\fP
Metric of the glyph (left\-bearing)\&. 
.PP
\fBint MDrawGlyph::rbearing\fP
Metric of the glyph (right\-bearing)\&. 
.PP
\fBint MDrawGlyph::ascent\fP
Metric of the glyph (ascent)\&. 
.PP
\fBint MDrawGlyph::descent\fP
Metric of the glyph (descent)\&. 
.PP
\fB\fBMFont\fP* MDrawGlyph::font\fP
Font used for the glyph\&. Set to NULL if no font is found for the glyph\&. 
.br
 
.PP
\fBMSymbol MDrawGlyph::font_type\fP
Type of the font\&. One of Mx, Mfreetype, Mxft\&. 
.PP
\fBvoid* MDrawGlyph::fontp\fP
Pointer to the font structure\&. The actual type is (XFontStruct *) if <font_type> member is Mx, FT_Face if <font_type> member is Mfreetype, and (XftFont *) if <font_type> member is Mxft\&. 
.br
 

.SH "Typedef Documentation"
.PP 
.SS "typedef void* \fBMDrawWindow\fP"

.PP
Window system dependent type for a window\&. The type \fBMDrawWindow\fP is for a window; a rectangular area that works in several ways like a miniature screen\&.
.PP
What it actually points depends on a window system\&. A program that uses the m17n\-X library must coerce the type \fCDrawable\fP to this type\&. 
.br
 
.SS "typedef void* \fBMDrawRegion\fP"

.PP
Window system dependent type for a region\&. The type \fBMDrawRegion\fP is for a region; an arbitrary set of pixels on the screen (typically a rectangular area)\&.
.PP
What it actually points depends on a window system\&. A program that uses the m17n\-X library must coerce the type \fCRegion\fP to this type\&. 
.br
 
.SH "Variable Documentation"
.PP 
.SS "int mdraw_line_break_option"

.PP
Option of line breaking for drawing text\&. The variable \fBmdraw_line_break_option\fP specifies line breaking options by logical\-or of the members of \fBMTextLineBreakOption\fP\&. It controls the line breaking algorithm of the function \fBmdraw_default_line_break()\fP\&. 
.br
 
.SH "Author"
.PP 
Generated automatically by Doxygen for The m17n Library from the source code\&.
.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
