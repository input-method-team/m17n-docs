.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mfont_list_family_names" 3m17n "Mon Sep 25 2023" "Version 1.8.4" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mfont_list_family_names \- 


.SH SYNOPSIS
\fBMPlist\fP*
\fBmfont_list_family_names\fP (\fBMFrame\fP *
\fIframe\fP)


.SH DESCRIPTION
.PP
Get a list of font famiy names\&. The
.ft B
mfont_list_family_names()
.ft R
functions returns a list of font family names available on frame
.ft B
frame\fP\&.
.PP
.SH RETURN VALUE
.PP
.RS 4

.RE
.PP
This function returns a plist whose keys are
.ft B
Msymbol
.ft R
and values are symbols representing font family names\&. The elements are sorted by alphabetical order\&. The plist must be freed by
.ft B
m17n_object_unref()\fP\&. If not font is found, it returns NULL\&. 
.br
 
.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
