.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "minput_create_ic" 3m17n "Mon Sep 25 2023" "Version 1.8.4" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
minput_create_ic \- 


.SH SYNOPSIS
\fBMInputContext\fP*
\fBminput_create_ic\fP (\fBMInputMethod\fP *
\fIim\fP, void *
\fIarg\fP)


.SH DESCRIPTION
.PP
Create an input context\&. The
.ft B
minput_create_ic()
.ft R
function creates an input context object associated with input method
.ft B
im\fP,
.ft R
and calls callback functions corresponding to
.ft B
Minput_preedit_start\fP,
.ft R
.ft B
Minput_status_start\fP,
.ft R
and
.ft B
Minput_status_draw
.ft R
in this order\&.
.PP
.SH RETURN VALUE
.PP
.RS 4
If an input context is successfully created,
.ft B
minput_create_ic()
.ft R
returns a pointer to it\&. Otherwise it returns
.ft C
NULL\fP\&. 
.br
 
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
