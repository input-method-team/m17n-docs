.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "minput_event_to_key" 3m17n "Mon Sep 25 2023" "Version 1.8.4" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
minput_event_to_key \- 


.SH SYNOPSIS
MSymbol
\fBminput_event_to_key\fP (\fBMFrame\fP *
\fIframe\fP, void *
\fIevent\fP)


.SH DESCRIPTION
.PP
Convert an event to an input key\&. The
.ft B
minput_event_to_key()
.ft R
function returns the input key corresponding to event
.ft B
event
.ft R
on
.ft B
frame
.ft R
by a window system dependent manner\&.
.PP
In the m17n\-X library,
.ft B
event
.ft R
must be a pointer to the structure
.ft C
XKeyEvent\fP,
.ft R
and it is handled as below\&.
.PP
At first, the keysym name of
.ft B
event
.ft R
is acquired by the function
.ft C
XKeysymToString\fP\&. Then, the name is modified as below\&.
.PP
If the name is one of 'a' \&.\&. 'z' and
.ft B
event
.ft R
has a Shift modifier, the name is converted to 'A' \&.\&. 'Z' respectively, and the Shift modifier is cleared\&.
.PP
If the name is one byte length and
.ft B
event
.ft R
has a Control modifier, the byte is bitwise anded by 0x1F and the Control modifier is cleared\&.
.PP
If
.ft B
event
.ft R
still has modifiers, the name is preceded by 'S\-' (Shift), 'C\-' (Control), 'M\-' (Meta), 'A\-' (Alt), 'G\-' (AltGr), 's\-' (Super), and 'H\-' (Hyper) in this order\&.
.PP
For instance, if the keysym name is 'a' and the event has Shift, Meta, and Hyper modifiers, the resulting name is 'M\-H\-A'\&.
.PP
At last, a symbol who has the name is returned\&. 
.br
 
.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
