.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mtext_cpy" 3m17n "Mon Sep 25 2023" "Version 1.8.4" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mtext_cpy \- 


.SH SYNOPSIS
\fBMText\fP*
\fBmtext_cpy\fP (\fBMText\fP *
\fImt1\fP, \fBMText\fP *
\fImt2\fP)


.SH DESCRIPTION
.PP
Copy an M\-text to another\&. The
.ft B
mtext_cpy()
.ft R
function copies M\-text
.ft B
mt2
.ft R
to M\-text
.ft B
mt1
.ft R
while inheriting all the text properties\&. The old text in
.ft B
mt1
.ft R
is overwritten and the length of
.ft B
mt1
.ft R
is extended if necessary\&.
.ft B
mt2
.ft R
is not modified\&.
.PP
.SH RETURN VALUE
.PP
.RS 4
This function returns a pointer to the resulting M\-text
.ft B
mt1\fP\&. 
.br
.RE
.PP
.SH "SEE ALSO"
.PP
.RS 4
\fBmtext_ncpy()\fP,
.ft R
.ft B
mtext_copy()
.ft R
.br
 
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
