.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mtext_push_property" 3m17n "Mon Sep 25 2023" "Version 1.8.4" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mtext_push_property \- 


.SH SYNOPSIS
int
\fBmtext_push_property\fP (\fBMText\fP *
\fImt\fP, int
\fIfrom\fP, int
\fIto\fP, \fBMTextProperty\fP *
\fIprop\fP)


.SH DESCRIPTION
.PP
Push a text property onto an M\-text\&. The
.ft B
mtext_push_property()
.ft R
function pushes text property
.ft B
prop
.ft R
to the characters between
.ft B
from
.ft R
(inclusive) and
.ft B
to
.ft R
(exclusive) in M\-text
.ft B
mt\fP\&.
.PP
.SH RETURN VALUE
.PP
.RS 4
If the operation was successful,
.ft B
mtext_push_property()
.ft R
returns 0\&. Otherwise it returns \-1 and assigns an error code to the external variable
.ft B
merror_code\fP\&. 
.br
 
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
