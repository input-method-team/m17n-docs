.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "msymbol_exist" 3m17n "Mon Sep 25 2023" "Version 1.8.4" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
msymbol_exist \- 


.SH SYNOPSIS
MSymbol
\fBmsymbol_exist\fP (const char *
\fIname\fP)


.SH DESCRIPTION
.PP
.nf
@brief Search for a symbol that has a specified name.

The msymbol_exist() function searches for the symbol whose name
is @b name.

@par Return value:
If such a symbol exists, msymbol_exist() returns that symbol.
Otherwise it returns the predefined symbol #Mnil.

@par Errors:
This function never fails.   

.fi
.fi
 
.PP
.SH "SEE ALSO"
.PP
.RS 4
\fBmsymbol_name()\fP,
.ft R
.ft B
msymbol()
.ft R
.br
 
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
