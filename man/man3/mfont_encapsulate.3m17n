.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mfont_encapsulate" 3m17n "Mon Sep 25 2023" "Version 1.8.4" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mfont_encapsulate \- 


.SH SYNOPSIS
\fBMFont\fP*
\fBmfont_encapsulate\fP (\fBMFrame\fP *
\fIframe\fP, MSymbol
\fIdata_type\fP, void *
\fIdata\fP)


.SH DESCRIPTION
.PP
Encapusulate a font\&. The
.ft B
mfont_encapsulate()
.ft R
functions realizes a font by encapusulating data
.ft B
data
.ft R
or type
.ft B
data_type
.ft R
on
.ft B
frame\fP\&. Currently
.ft B
data_tape
.ft R
is
.ft B
Mfontconfig
.ft R
or
.ft B
Mfreetype\fP,
.ft R
and
.ft B
data
.ft R
points to an object of FcPattern or FT_Face respectively\&.
.PP
.SH RETURN VALUE
.PP
.RS 4
If the operation was successful, a realized font is returned\&. Otherwise NULL is return\&.
.RE
.PP
.SH "SEE ALSO"
.PP
.RS 4
\fBmfont_close()\fP\&. 
.br
 
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
