.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "m17nIntro" 3m17n "Mon Sep 25 2023" "Version 1.8.4" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
m17nIntro_\-_Introduc \-  \- Introduction to the m17n library\&.  

.SH SYNOPSIS
.br
.PP
.SS "Macros"

.in +1c
.ti -1c
.RI "#define \fBM17NLIB_MAJOR_VERSION\fP"
.br
.ti -1c
.RI "#define \fBM17NLIB_MINOR_VERSION\fP"
.br
.ti -1c
.RI "#define \fBM17NLIB_PATCH_LEVEL\fP"
.br
.ti -1c
.RI "#define \fBM17NLIB_VERSION_NAME\fP"
.br
.ti -1c
.RI "#define \fBM17N_INIT\fP()"
.br
.RI "Initialize the m17n library\&. "
.ti -1c
.RI "#define \fBM17N_FINI\fP()"
.br
.RI "Finalize the m17n library\&. "
.in -1c
.SS "Enumerations"

.in +1c
.ti -1c
.RI "enum \fBM17NStatus\fP { \fBM17N_NOT_INITIALIZED\fP, \fBM17N_CORE_INITIALIZED\fP, \fBM17N_SHELL_INITIALIZED\fP, \fBM17N_GUI_INITIALIZED\fP }"
.br
.RI "Enumeration for the status of the m17n library\&. "
.in -1c
.SS "Functions"

.in +1c
.ti -1c
.RI "enum \fBM17NStatus\fP \fBm17n_status\fP (void)"
.br
.RI "Report which part of the m17n library is initialized\&. "
.in -1c
.SH "Detailed Description"
.PP 
Introduction to the m17n library\&. 

\fIAPI LEVELS\fP
.PP
The API of the m17n library is divided into these five\&.
.PP
.PD 0
.IP "1." 4
CORE API
.PP
It provides basic modules to handle M\-texts\&. To use this API, an application program must include <m17n\-core\fI\fP\&.h> and be linked with \-lm17n\-core\&.
.PP

.IP "2." 4
SHELL API
.PP
It provides modules for character properties, character set handling, code conversion, etc\&. They load various kinds of data from the database on demand\&. To use this API, an application program must include <m17n\fI\fP\&.h> and be linked with \-lm17n\-core \-lm17n\&.
.PP
When you use this API, CORE API is also available\&.
.PP

.IP "3." 4
FLT API
.PP
It provides modules for text shaping using \fBmdbFLT\fP\&. To use this API, an application program must include <m17n\fI\fP\&.h> and be linked with \-lm17n\-core \-lm17n\-flt\&.
.PP
When you use this API, CORE API is also available\&.
.PP

.IP "4." 4
GUI API
.PP
It provides GUI modules such as drawing and inputting M\-texts on a graphic device\&. This API itself is independent of graphic devices, but most functions require an argument \fBMFrame\fP that is created for a specific type of graphic devices\&. The currently supported graphic devices are null device, the X Window System, and image data (gdImagePtr) of the GD library\&.
.PP
On a frame of a null device, you cannot draw text nor use input methods\&. However, functions like \fBmdraw_glyph_list()\fP, etc\&. are available\&.
.PP
On a frame of the X Window System, you can use the whole GUI API\&.
.PP
On a frame of the GD library, you can use all drawing API but cannot use input methods\&.
.PP
To use this API, an application program must include <m17n\-gui\fI\fP\&.h> and be linked with \-lm17n\-core \-lm17n \-lm17n\-gui\&.
.PP
When you use this API, CORE, SHELL, and FLT APIs are also available\&.
.PP

.IP "5." 4
MISC API
.PP
It provides miscellaneous functions to support error handling and debugging\&. This API cannot be used standalone; it must be used with one or more APIs listed above\&. To use this API, an application program must include <m17n\-misc\fI\fP\&.h> in addition to one of the header files described above\&.
.PP

.PP
.PP
See also the section \fBm17n\-config(1)\fP\&.
.PP
\fIENVIRONMENT VARIABLES\fP
.PP
The m17n library pays attention to the following environment variables\&.
.PP
.PD 0
.IP "\(bu" 2
\fCM17NDIR\fP 
.PP
The name of the directory that contains data of the m17n database\&. See \fBDatabase\fP for details\&.
.PP

.IP "\(bu" 2
\fCMDEBUG_XXX\fP 
.PP
Environment variables whose names start with 'MDEBUG_' control debug information output\&. See \fBDebugging\fP for details\&.
.PP

.PP
.PP
\fIAPI NAMING CONVENTION\fP
.PP
The m17n library exports functions, variables, macros, and types\&. All of them start with the letter 'm' or 'M', and are followed by an object name (e\&.g\&. 'symbol', 'plist') or a module name (e\&.g\&. draw, input)\&. Note that the name of M\-text objects start with 'mtext' and not with 'mmtext'\&.
.PP
.PD 0
.IP "\(bu" 2
functions \-\- mobject() or mobject_xxx()
.PP
They start with 'm' and are followed by an object name in lower case\&. Words are separated by '_'\&. For example, \fBmsymbol()\fP, \fBmtext_ref_char()\fP, \fBmdraw_text()\fP\&.
.PP

.IP "\(bu" 2
non\-symbol variables \-\- mobject, or mobject_xxx
.PP
The naming convention is the same as functions (e\&.g\&. mface_large)\&.
.PP

.IP "\(bu" 2
symbol variables \-\- Mname
.PP
Variables of the type MSymbol start with 'M' and are followed by their names\&. Words are separated by '_'\&. For example, Mlanguage (the name is 'language'), Miso_2022 (the name is 'iso\-2022')\&.
.PP

.IP "\(bu" 2
macros \-\- MOBJECT_XXX
.PP
They start with 'M' and are followed by an object name in upper case\&. Words are separated by '_'\&.
.PP

.IP "\(bu" 2
types \-\- MObject or MObjectXxx
.PP
They start with 'M' and are followed by capitalized object names\&. Words are concatenated directly and no '_' are used\&. For example, \fBMConverter\fP, \fBMInputDriver\fP\&.
.PP

.PP

.SH "Macro Definition Documentation"
.PP 
.SS "#define M17NLIB_MAJOR_VERSION"
The \fBM17NLIB_MAJOR_VERSION\fP macro gives the major version number of the m17n library\&. 
.br
 
.SS "#define M17NLIB_MINOR_VERSION"
The \fBM17NLIB_MINOR_VERSION\fP macro gives the minor version number of the m17n library\&. 
.br
 
.SS "#define M17NLIB_PATCH_LEVEL"
The \fBM17NLIB_PATCH_LEVEL\fP macro gives the patch level number of the m17n library\&. 
.br
 
.SS "#define M17NLIB_VERSION_NAME"
The \fBM17NLIB_VERSION_NAME\fP macro gives the version name of the m17n library as a string\&. 
.br
 
.SS "#define M17N_INIT()"

.PP
Initialize the m17n library\&. The macro \fBM17N_INIT()\fP initializes the m17n library\&. This macro must be called before any m17n functions are used\&.
.PP
It is safe to call this macro multiple times, but in that case, the macro \fBM17N_FINI()\fP must be called the same times to free the memory\&.
.PP
If the initialization was successful, the external variable \fBmerror_code\fP is set to 0\&. Otherwise it is set to \-1\&.
.PP

\fBSEE ALSO\fp

.RS 4
\fBM17N_FINI()\fP, \fBm17n_status()\fP 
.br
 
.RE
.PP

.SS "#define M17N_FINI()"

.PP
Finalize the m17n library\&. The macro \fBM17N_FINI()\fP finalizes the m17n library\&. It frees all the memory area used by the m17n library\&. Once this macro is called, no m17n functions should be used until the macro \fBM17N_INIT()\fP is called again\&.
.PP
If the macro \fBM17N_INIT()\fP was called N times, the Nth call of this macro actually free the memory\&.
.PP

\fBSEE ALSO\fp

.RS 4
\fBM17N_INIT()\fP, \fBm17n_status()\fP 
.br
 
.RE
.PP

.SH "Enumeration Type Documentation"
.PP 
.SS "enum \fBM17NStatus\fP"

.PP
Enumeration for the status of the m17n library\&. The enum \fBM17NStatus\fP is used as a return value of the function \fBm17n_status()\fP\&. 
.br
 
.PP
\fBEnumerator\fP
.in +1c
.TP
\fB\fIM17N_NOT_INITIALIZED \fP\fP
No modules is initialized, and all modules are finalized\&. 
.TP
\fB\fIM17N_CORE_INITIALIZED \fP\fP
Only the modules in CORE API are initialized\&. 
.TP
\fB\fIM17N_SHELL_INITIALIZED \fP\fP
Only the modules in CORE and SHELL APIs are initialized\&. 
.TP
\fB\fIM17N_GUI_INITIALIZED \fP\fP
All modules are initialized\&. 
.SH "Author"
.PP 
Generated automatically by Doxygen for The m17n Library from the source code\&.
.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
