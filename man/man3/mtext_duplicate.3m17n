.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mtext_duplicate" 3m17n "Mon Sep 25 2023" "Version 1.8.4" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mtext_duplicate \- 


.SH SYNOPSIS
\fBMText\fP*
\fBmtext_duplicate\fP (\fBMText\fP *
\fImt\fP, int
\fIfrom\fP, int
\fIto\fP)


.SH DESCRIPTION
.PP
Create a new M\-text from a part of an existing M\-text\&. The
.ft B
mtext_duplicate()
.ft R
function creates a copy of sub\-text of M\-text
.ft B
mt\fP,
.ft R
starting at
.ft B
from
.ft R
(inclusive) and ending at
.ft B
to
.ft R
(exclusive) while inheriting all the text properties of
.ft B
mt\fP\&.
.ft B
mt
.ft R
itself is not modified\&.
.PP
.SH RETURN VALUE
.PP
.RS 4
If the operation was successful,
.ft B
mtext_duplicate()
.ft R
returns a pointer to the created M\-text\&. If an error is detected, it returns NULL and assigns an error code to the external variable
.ft B
merror_code\fP\&. 
.br
.RE
.PP
.SH ERRORS
.PP
.RS 4
\fCMERROR_RANGE
.ft R
.RE
.PP
.SH "SEE ALSO"
.PP
.RS 4
\fBmtext_dup()
.ft R
.br
 
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
