.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mfont_put_prop" 3m17n "Mon Sep 25 2023" "Version 1.8.4" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mfont_put_prop \- 


.SH SYNOPSIS
int
\fBmfont_put_prop\fP (\fBMFont\fP *
\fIfont\fP, MSymbol
\fIkey\fP, void *
\fIval\fP)


.SH DESCRIPTION
.PP
Put a property value to a font\&. The
.ft B
mfont_put_prop()
.ft R
function puts a font property whose key is
.ft B
key
.ft R
and value is
.ft B
val
.ft R
to font
.ft B
font\fP\&.
.ft B
key
.ft R
must be one of the following symbols:
.PP
\fCMfoundry\fP,
.ft R
.ft C
Mfamily\fP,
.ft R
.ft C
Mweight\fP,
.ft R
.ft C
Mstyle\fP,
.ft R
.ft C
Mstretch\fP,
.ft R
.ft C
Madstyle\fP,
.ft R
.ft C
Mregistry\fP,
.ft R
.ft C
Msize\fP,
.ft R
.ft C
Mresolution\fP\&.
.PP
If
.ft B
key
.ft R
is
.ft C
Msize
.ft R
or
.ft C
Mresolution\fP,
.ft R
.ft B
val
.ft R
must be an integer\&. Otherwise,
.ft B
val
.ft R
must be a symbol of a property value name\&. But, if the name is 'nil', a symbol of name 'Nil' must be specified\&. 
.br
 
.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
