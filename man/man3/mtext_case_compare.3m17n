.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mtext_case_compare" 3m17n "Mon Sep 25 2023" "Version 1.8.4" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mtext_case_compare \- 


.SH SYNOPSIS
int
\fBmtext_case_compare\fP (\fBMText\fP *
\fImt1\fP, int
\fIfrom1\fP, int
\fIto1\fP, \fBMText\fP *
\fImt2\fP, int
\fIfrom2\fP, int
\fIto2\fP)


.SH DESCRIPTION
.PP
Compare specified regions of two M\-texts ignoring cases\&. The
.ft B
mtext_case_compare()
.ft R
function compares two M\-texts
.ft B
mt1
.ft R
and
.ft B
mt2\fP,
.ft R
character\-by\-character, ignoring cases\&. The compared regions are between
.ft B
from1
.ft R
and
.ft B
to1
.ft R
in
.ft B
mt1
.ft R
and
.ft B
from2
.ft R
to
.ft B
to2
.ft R
in MT2\&.
.ft B
from1
.ft R
and
.ft B
from2
.ft R
are inclusive,
.ft B
to1
.ft R
and
.ft B
to2
.ft R
are exclusive\&.
.ft B
from1
.ft R
being equal to
.ft B
to1
.ft R
(or
.ft B
from2
.ft R
being equal to
.ft B
to2\fP) means an M\-text of length zero\&. An invalid region specification is regarded as both
.ft B
from1
.ft R
and
.ft B
to1
.ft R
(or
.ft B
from2
.ft R
and
.ft B
to2\fP) being 0\&.
.PP
.SH RETURN VALUE
.PP
.RS 4
This function returns 1, 0, or \-1 if
.ft B
mt1
.ft R
is found greater than, equal to, or less than
.ft B
mt2\fP,
.ft R
respectively\&. Comparison is based on character codes\&. 
.br
.RE
.PP
.SH "SEE ALSO"
.PP
.RS 4
\fBmtext_cmp()\fP,
.ft R
.ft B
mtext_ncmp()\fP,
.ft R
.ft B
mtext_casecmp()\fP,
.ft R
.ft B
mtext_ncasecmp()\fP,
.ft R
.ft B
mtext_compare()
.ft R
.br
 
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
