.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mdbTutorialIM" 5 "Mon Sep 25 2023" "Version 1.8.4" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mdbTutorialIM \- Tutorial of input method 

.SH "Structure of an input method file"
.PP
An input method is defined in a *\&.mim file with this format\&.
.PP
.PP
.nf
(input\-method LANG NAME)

(description (_ "DESCRIPTION"))

(title "TITLE\-STRING")

(map
  (MAP\-NAME
    (KEYSEQ MAP\-ACTION MAP\-ACTION ...)        <\- rule
    (KEYSEQ MAP\-ACTION MAP\-ACTION ...)        <\- rule
    ...)
  (MAP\-NAME
    (KEYSEQ MAP\-ACTION MAP\-ACTION ...)        <\- rule
    (KEYSEQ MAP\-ACTION MAP\-ACTION ...)        <\- rule
    ...)
  ...)

(state
  (STATE\-NAME
    (MAP\-NAME BRANCH\-ACTION BRANCH\-ACTION ...)   <\- branch
    ...)
  (STATE\-NAME
    (MAP\-NAME BRANCH\-ACTION BRANCH\-ACTION ...)   <\- branch
    ...)
  ...)
.fi
.PP
Lowercase letters and parentheses are literals, so they must be written as they are\&. Uppercase letters represent arbitrary strings\&.
.PP
KEYSEQ specifies a sequence of keys in this format: 
.PP
.nf
  (SYMBOLIC\-KEY SYMBOLIC\-KEY ...)

.fi
.PP
where SYMBOLIC\-KEY is the keysym value returned by the xev command\&. For instance 
.PP
.nf
  (n i)

.fi
.PP
represents a key sequence of <n> and <i>\&. If all SYMBOLIC\-KEYs are ASCII characters, you can use the short form 
.PP
.nf
  "ni"

.fi
.PP
instead\&. Consult \fBInput Method\fP for Non\-ASCII characters\&.
.PP
Both MAP\-ACTION and BRANCH\-ACTION are a sequence of actions of this format: 
.PP
.nf
  (ACTION ARG ARG ...)

.fi
.PP
The most common action is \fCinsert\fP, which is written as this: 
.PP
.nf
  (insert "TEXT")

.fi
.PP
But as it is very frequently used, you can use the short form 
.PP
.nf
  "TEXT"

.fi
.PP
If \fC'TEXT'\fP contains only one character 'C', you can write it as 
.PP
.nf
  (insert ?C)

.fi
.PP
or even shorter as 
.PP
.nf
  ?C

.fi
.PP
So the shortest notation for an action of inserting 'a' is 
.PP
.nf
  ?a

.fi
.PP
.SH "Simple example of capslock"
.PP
Here is a simple example of an input method that works as CapsLock\&.
.PP
.PP
.nf
(input\-method en capslock)
(description (_ "Upcase all lowercase letters"))
(title "a\->A")
(map
  (toupper ("a" "A") ("b" "B") ("c" "C") ("d" "D") ("e" "E")
           ("f" "F") ("g" "G") ("h" "H") ("i" "I") ("j" "J")
           ("k" "K") ("l" "L") ("m" "M") ("n" "N") ("o" "O")
           ("p" "P") ("q" "Q") ("r" "R") ("s" "S") ("t" "T")
           ("u" "U") ("v" "V") ("w" "W") ("x" "X") ("y" "Y")
           ("z" "Z")))
(state
  (init (toupper)))
.fi
.PP
.PP
When this input method is activated, it is in the initial condition of the first state (in this case, the only state \fCinit\fP)\&. In the initial condition, no key is being processed and no action is suspended\&. When the input method receives a key event <a>, it searches branches in the current state for a rule that matches <a> and finds one in the map \fCtoupper\fP\&. Then it executes MAP\-ACTIONs (in this case, just inserting 'A' in the preedit buffer)\&. After all MAP\-ACTIONs have been executed, the input method shifts to the initial condition of the current state\&.
.PP
The shift to \fIthe initial condition of the first state\fP has a special meaning; it commits all characters in the preedit buffer then clears the preedit buffer\&.
.PP
As a result, 'A' is given to the application program\&.
.PP
When a key event does not match with any rule in the current state, that event is unhandled and given back to the application program\&.
.PP
Turkish users may want to extend the above example for 'İ' (U+0130: LATIN CAPITAL LETTER I WITH DOT ABOVE)\&. It seems that assigning the key sequence <i> <i> for that character is convenient\&. So, he will add this rule in \fCtoupper\fP\&.
.PP
.PP
.nf
    ("ii" "İ")
.fi
.PP
.PP
However, we already have the following rule:
.PP
.PP
.nf
    ("i" "I")
.fi
.PP
.PP
What will happen when a key event <i> is sent to the input method?
.PP
No problem\&. When the input method receives <i>, it inserts 'I' in the preedit buffer\&. It knows that there is another rule that may match the additional key event <i>\&. So, after inserting 'I', it suspends the normal behavior of shifting to the initial condition, and waits for another key\&. Thus, the user sees 'I' with underline, which indicates it is not yet committed\&.
.PP
When the input method receives the next <i>, it cancels the effects done by the rule for the previous 'i' (in this case, the preedit buffer is cleared), and executes MAP\-ACTIONs of the rule for 'ii'\&. So, 'İ' is inserted in the preedit buffer\&. This time, as there are no other rules that match with an additional key, it shifts to the initial condition of the current state, which leads to commit 'İ'\&.
.PP
Then, what will happen when the next key event is <a> instead of <i>?
.PP
No problem, either\&.
.PP
The input method knows that there are no rules that match the <i> <a> key sequence\&. So, when it receives the next <a>, it executes the suspended behavior (i\&.e\&. shifting to the initial condition), which leads to commit 'I'\&. Then the input method tries to handle <a> in the current state, which leads to commit 'A'\&.
.PP
So far, we have explained MAP\-ACTION, but not BRANCH\-ACTION\&. The format of BRANCH\-ACTION is the same as that of MAP\-ACTION\&. It is executed only after a matching rule has been determined and the corresponding MAP\-ACTIONs have been executed\&. A typical use of BRANCH\-ACTION is to shift to a different state\&.
.PP
To see this effect, let us modify the current input method to upcase only word\-initial letters (i\&.e\&. to capitalize)\&. For that purpose, we modify the 'init' state as this:
.PP
.PP
.nf
  (init
    (toupper (shift non\-upcase)))
.fi
.PP
.PP
Here \fC(shift non\-upcase)\fP is an action to shift to the new state \fCnon\-upcase\fP, which has two branches as below:
.PP
.PP
.nf
  (non\-upcase
    (lower)
    (nil (shift init)))
.fi
.PP
.PP
The first branch is simple\&. We can define the new map \fClower\fP as the following to insert lowercase letters as they are\&.
.PP
.PP
.nf
(map
  ...
  (lower ("a" "a") ("b" "b") ("c" "c") ("d" "d") ("e" "e")
         ("f" "f") ("g" "g") ("h" "h") ("i" "i") ("j" "j")
         ("k" "k") ("l" "l") ("m" "m") ("n" "n") ("o" "o")
         ("p" "p") ("q" "q") ("r" "r") ("s" "s") ("t" "t")
         ("u" "u") ("v" "v") ("w" "w") ("x" "x") ("y" "y")
         ("z" "z")))
.fi
.PP
.PP
The second branch has a special meaning\&. The map name \fCnil\fP means that it matches with any key event that does not match any rules in the other maps in the current state\&. In addition, it does not consume any key event\&. We will show the full code of the new input method before explaining how it works\&.
.PP
.PP
.nf
(input\-method en titlecase)
(description (_ "Titlecase letters"))
(title "abc\->Abc")
(map
  (toupper ("a" "A") ("b" "B") ("c" "C") ("d" "D") ("e" "E")
           ("f" "F") ("g" "G") ("h" "H") ("i" "I") ("j" "J")
           ("k" "K") ("l" "L") ("m" "M") ("n" "N") ("o" "O")
           ("p" "P") ("q" "Q") ("r" "R") ("s" "S") ("t" "T")
           ("u" "U") ("v" "V") ("w" "W") ("x" "X") ("y" "Y")
           ("z" "Z") ("ii" "İ"))
  (lower ("a" "a") ("b" "b") ("c" "c") ("d" "d") ("e" "e")
         ("f" "f") ("g" "g") ("h" "h") ("i" "i") ("j" "j")
         ("k" "k") ("l" "l") ("m" "m") ("n" "n") ("o" "o")
         ("p" "p") ("q" "q") ("r" "r") ("s" "s") ("t" "t")
         ("u" "u") ("v" "v") ("w" "w") ("x" "x") ("y" "y")
         ("z" "z")))
(state
  (init
    (toupper (shift non\-upcase)))
  (non\-upcase
    (lower (commit))
    (nil (shift init))))
.fi
.PP
.PP
Let's see what happens when the user types the key sequence <a> <b> < >\&. Upon <a>, 'A' is inserted into the buffer and the state shifts to \fCnon\-upcase\fP\&. So, the next <b> is handled in the \fCnon\-upcase\fP state\&. As it matches a rule in the map \fClower\fP, 'b' is inserted in the preedit buffer and characters in the buffer ('Ab') are committed explicitly by the 'commit' command in BRANCH\-ACTION\&. After that, the input method is still in the \fCnon\-upcase\fP state\&. So the next < > is also handled in \fCnon\-upcase\fP\&. For this time, no rule in this state matches it\&. Thus the branch \fC(nil (shift init))\fP is selected and the state is shifted to \fCinit\fP\&. Please note that < > is not yet handled because the map \fCnil\fP does not consume any key event\&. So, the input method tries to handle it in the \fCinit\fP state\&. Again no rule matches it\&. Therefore, that event is given back to the application program, which usually inserts a space for that\&.
.PP
When you type 'a quick blown fox' with this input method, you get 'A
Quick Blown Fox'\&. OK, you find a typo in 'blown', which should be 'brown'\&. To correct it, you probably move the cursor after 'l' and type <Backspace> and <r>\&. However, if the current input method is still active, a capital 'R' is inserted\&. It is not a sophisticated behavior\&.
.SH "Example of utilizing surrounding text support"
.PP
To make the input method work well also in such a case, we must use 'surrounding text support'\&. It is a way to check characters around the inputting spot and delete them if necessary\&. Note that this facility is available only with Gtk+ applications and Qt applications\&. You cannot use it with applications that use XIM to communicate with an input method\&.
.PP
Before explaining how to utilize 'surrounding text support', you must understand how to use variables, arithmetic comparisons, and conditional actions\&.
.PP
At first, any symbol (except for several preserved ones) used as ARG of an action is treated as a variable\&. For instance, the commands
.PP
.PP
.nf
  (set X 32) (insert X)
.fi
.PP
.PP
set the variable \fCX\fP to integer value 32, then insert a character whose Unicode character code is 32 (i\&.e\&. SPACE)\&.
.PP
The second argument of the \fCset\fP action can be an expression of this form:
.PP
.PP
.nf
  (OPERATOR ARG1 [ARG2])
.fi
.PP
.PP
Both ARG1 and ARG2 can be an expression\&. So,
.PP
.PP
.nf
  (set X (+ (* Y 32) Z))
.fi
.PP
.PP
sets \fCX\fP to the value of \fCY * 32 + Z\fP\&.
.PP
We have the following arithmetic/bitwise OPERATORs (require two arguments):
.PP
.PP
.nf
  + \- * / & |
.fi
.PP
.PP
these relational OPERATORs (require two arguments):
.PP
.PP
.nf
  == <= >= < >
.fi
.PP
.PP
and this logical OPERATOR (requires one argument):
.PP
.PP
.nf
  !
.fi
.PP
.PP
For surrounding text support, we have these preserved variables:
.PP
.PP
.nf
  @\-0, @\-N, @+N (N is a positive integer)
.fi
.PP
.PP
The values of them are predefined as below and can not be altered\&.
.PP
.PD 0
.IP "\(bu" 2
\fC\-0\fP
.PP
\-1 if surrounding text is supported, \-2 if not\&.
.PP

.IP "\(bu" 2
\fC\-N\fP
.PP
The Nth previous character in the preedit buffer\&. If there are only M (M<N) previous characters in it, the value is the (N\-M)th previous character from the inputting spot\&.
.PP

.IP "\(bu" 2
\fC+N\fP
.PP
The Nth following character in the preedit buffer\&. If there are only M (M<N) following characters in it, the value is the (N\-M)th following character from the inputting spot\&.
.PP

.PP
.PP
So, provided that you have this context:
.PP
.PP
.nf
  ABC|def|GHI
.fi
.PP
.PP
('def' is in the preedit buffer, two '|'s indicate borders between the preedit buffer and the surrounding text) and your current position in the preedit buffer is between 'd' and 'e', you get these values:
.PP
.PP
.nf
  @\-3 \-\- ?B
  @\-2 \-\- ?C
  @\-1 \-\- ?d
  @+1 \-\- ?e
  @+2 \-\- ?f
  @+3 \-\- ?G
.fi
.PP
.PP
Next, you have to understand the conditional action of this form:
.PP
.PP
.nf
  (cond
    (EXPR1 ACTION ACTION ...)
    (EXPR2 ACTION ACTION ...)
    ...)
.fi
.PP
.PP
where EXPRn are expressions\&. When an input method executes this action, it resolves the values of EXPRn one by one from the first branch\&. If the value of EXPRn is resolved into nonzero, the corresponding actions are executed\&.
.PP
Now you are ready to write a new version of the input method 'Titlecase'\&.
.PP
.PP
.nf
(input\-method en titlecase2)
(description (_ "Titlecase letters"))
(title "abc\->Abc")
(map
  (toupper ("a" "A") ("b" "B") ("c" "C") ("d" "D") ("e" "E")
           ("f" "F") ("g" "G") ("h" "H") ("i" "I") ("j" "J")
           ("k" "K") ("l" "L") ("m" "M") ("n" "N") ("o" "O")
           ("p" "P") ("q" "Q") ("r" "R") ("s" "S") ("t" "T")
           ("u" "U") ("v" "V") ("w" "W") ("x" "X") ("y" "Y")
           ("z" "Z") ("ii" "İ")))
(state
  (init
    (toupper

     ;; Now we have exactly one uppercase character in the preedit
     ;; buffer.  So, "@\-2" is the character just before the inputting
     ;; spot.

     (cond ((| (& (>= @\-2 ?A) (<= @\-2 ?Z))
               (& (>= @\-2 ?a) (<= @\-2 ?z))
               (= @\-2 ?İ))

        ;; If the character before the inputting spot is A..Z,
        ;; a..z, or İ, remember the only character in the preedit
        ;; buffer in the variable X and delete it.

        (set X @\-1) (delete @\-)

        ;; Then insert the lowercase version of X.

        (cond ((= X ?İ) "i") 
                  (1 (set X (+ X 32)) (insert X))))))))
.fi
.PP
.PP
The above example contains the new action \fCdelete\fP\&. So, it is time to explain more about the preedit buffer\&. The preedit buffer is a temporary place to store a sequence of characters\&. In this buffer, the input method keeps a position called the 'current position'\&. The current position exists between two characters, at the beginning of the buffer, or at the end of the buffer\&. The \fCinsert\fP action inserts characters before the current position\&. For instance, when your preedit buffer contains 'ab\&.c' ('\&.' indicates the current position),
.PP
.PP
.nf
  (insert "xyz")
.fi
.PP
.PP
changes the buffer to 'abxyz\&.c'\&.
.PP
There are several predefined variables that represent a specific position in the preedit buffer\&. They are:
.PP
.PD 0
.IP "\(bu" 2
\fC@<, @=, @>\fP
.PP
The first, current, and last positions\&.
.PP

.IP "\(bu" 2
\fC@\-, @+\fP
.PP
The previous and the next positions\&. 
.PP
.PP
The format of the \fCdelete\fP action is this:
.PP
.PP
.nf
  (delete POS)
.fi
.PP
.PP
where POS is a predefined positional variable\&. The above action deletes the characters between POS and the current position\&. So, \fC(delete \-)\fP deletes one character before the current position\&. The other examples of \fCdelete\fP include the followings:
.PP
.PP
.nf
  (delete @+)  ; delete the next character
  (delete @<)  ; delete all the preceding characters in the buffer
  (delete @>)  ; delete all the following characters in the buffer
.fi
.PP
.PP
You can change the current position using the \fCmove\fP action as below:
.PP
.PP
.nf
  (move @\-)  ; move the current position to the position before the
               previous character
  (move @<)  ; move to the first position
.fi
.PP
.PP
Other positional variables work similarly\&.
.PP
Let's see how our new example works\&. Whatever a key event is, the input method is in its only state, \fCinit\fP\&. Since an event of a lower letter key is firstly handled by MAP\-ACTIONs, every key is changed into the corresponding uppercase and put into the preedit buffer\&. Now this character can be accessed with \fC\-1\fP\&.
.PP
How can we tell whether the new character should be a lowercase or an uppercase? We can do so by checking the character before it, i\&.e\&. \fC\-2\fP\&. BRANCH\-ACTIONs in the \fCinit\fP state do the job\&.
.PP
It first checks if the character \fC\-2\fP is between A to Z, between a to z, or İ by the conditional below\&.
.PP
.PP
.nf
     (cond ((| (& (>= @\-2 ?A) (<= @\-2 ?Z))
               (& (>= @\-2 ?a) (<= @\-2 ?z))
               (= @\-2 ?İ))
.fi
.PP
.PP
If not, there is nothing to do specially\&. If so, our new key should be changed back into lowercase\&. Since the uppercase character is already in the preedit buffer, we retrieve and remember it in the variable \fCX\fP by
.PP
.PP
.nf
    (set X @\-1)
.fi
.PP
.PP
and then delete that character by
.PP
.PP
.nf
    (delete @\-)
.fi
.PP
.PP
Lastly we re\-insert the character in its lowercase form\&. The problem here is that 'İ' must be changed into 'i', so we need another conditional\&. The first branch
.PP
.PP
.nf
    ((= X ?İ) "i")
.fi
.PP
.PP
means that 'if the character remembered in X is 'İ', 'i' is inserted'\&.
.PP
The second branch
.PP
.PP
.nf
    (1 (set X (+ X 32)) (insert X))
.fi
.PP
.PP
starts with '1', which is always resolved into nonzero, so this branch is a catchall\&. Actions in this branch increase \fCX\fP by 32, then insert \fCX\fP\&. In other words, they change A\&.\&.\&.Z into a\&.\&.\&.z respectively and insert the resulting lowercase character into the preedit buffer\&. As the input method reaches the end of the BRANCH\-ACTIONs, the character is committed\&.
.PP
This new input method always checks the character before the current position, so 'A Quick Blown Fox' will be successfully fixed to 'A
Quick Brown Fox' by the key sequence <BackSpace> <r>\&. 
.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
