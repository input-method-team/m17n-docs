var searchData=
[
  ['u_1425',['u',['../structM17NObject.html#a8d897889f0fb0019b4e81dc4371a2528',1,'M17NObject']]],
  ['uint_5fsize_1426',['UINT_SIZE',['../character_8h.html#a7192e3d264590a1e53268a7d0ded58f7',1,'character.h']]],
  ['unified_5fmax_1427',['unified_max',['../structMCharset.html#ae5138d3fde515cccd8f2c64837da42ee',1,'MCharset']]],
  ['union_5frect_5fwith_5fregion_1428',['union_rect_with_region',['../structMDeviceDriver.html#aa423cdb20fa007bce031d0facfa8426c',1,'MDeviceDriver']]],
  ['uniq_5ffile_1429',['uniq_file',['../structMDatabaseInfo.html#a41cb3d414cea6b8f6c0e881faa562645',1,'MDatabaseInfo']]],
  ['unit_5fbytes_1430',['UNIT_BYTES',['../character_8h.html#a56505d898d951e3face15f27a51deb53',1,'character.h']]],
  ['unmap_5fwindow_1431',['unmap_window',['../structMDeviceDriver.html#a2820f41acddee439d7500588f9f5abf2',1,'MDeviceDriver']]],
  ['use_5fsafe_5falloca_1432',['USE_SAFE_ALLOCA',['../internal_8h.html#a4e046c98b60e290462d9bcd141a681da',1,'internal.h']]],
  ['used_1433',['used',['../structMFLTGlyphString.html#a3ccc7a1cea756cdd43178fb6d8b663e3',1,'MFLTGlyphString::used()'],['../structMCharsetISO2022Table.html#a146206322fcf653a3c6c31a9c26437df',1,'MCharsetISO2022Table::used()'],['../structMFontPropertyTable.html#ab3c6d85b1663e7195e0b1b1cc4a59017',1,'MFontPropertyTable::used()'],['../structMInputContextInfo.html#aa4df5d9dd6bd0de50f01f9edd5113a63',1,'MInputContextInfo::used()'],['../structMGlyphString.html#a6874ffcfd626e9afccb1e26a206ebc6d',1,'MGlyphString::used()'],['../structM17NObjectRecord.html#a2b7ad3246b19209148ef334801196667',1,'M17NObjectRecord::used()'],['../struct__M17NObjectArray.html#a3ebf321bfb31d1fdbd06c057aef821de',1,'_M17NObjectArray::used()']]],
  ['ushort_5fsize_1434',['USHORT_SIZE',['../character_8h.html#a236b6ca0ba9de7d754d07bb1e8c421fb',1,'character.h']]]
];
