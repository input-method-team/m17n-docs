var indexSectionsWithContent =
{
  0: "_abcdefghiklmnoprstuvwxy",
  1: "_m",
  2: "cdefilmpst",
  3: "dm",
  4: "abcdefghiklmnoprstuvwxy",
  5: "mo",
  6: "gm",
  7: "gm",
  8: "_acdefgimprstu",
  9: "cdefgilmpst",
  10: "dgpst"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "groups",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Modules",
  10: "Pages"
};

