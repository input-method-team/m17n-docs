var searchData=
[
  ['g_2061',['g',['../structMGlyph.html#aff6661e8fba13ecb76ede3f015e26b0e',1,'MGlyph']]],
  ['get_5fglyph_5fid_2062',['get_glyph_id',['../structMFLTFont.html#a187c3b748ab1b24576ffbc403e6aa36a',1,'MFLTFont']]],
  ['get_5fmetrics_2063',['get_metrics',['../structMFLTFont.html#ac9264cefa810c378061758450ea6a29c',1,'MFLTFont']]],
  ['get_5fprop_2064',['get_prop',['../structMDeviceDriver.html#a0bfb530b3ee5f42eeb96a104a3cd858f',1,'MDeviceDriver']]],
  ['glyph_5fcode_2065',['glyph_code',['../structMDrawGlyph.html#aebd827c9ab5b6cfda97ff1d150594d8c',1,'MDrawGlyph']]],
  ['glyph_5fsize_2066',['glyph_size',['../structMFLTGlyphString.html#a3f4914499360ee6085733370a7827993',1,'MFLTGlyphString']]],
  ['glyphs_2067',['glyphs',['../structMFLTGlyphString.html#af019589ab90ca6672fb31b65b7077af9',1,'MFLTGlyphString::glyphs()'],['../structMGlyphString.html#abe9b33d761cc547fa0f1e0db1c55cf47',1,'MGlyphString::glyphs()']]]
];
