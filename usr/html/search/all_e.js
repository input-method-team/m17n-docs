var searchData=
[
  ['object_1297',['object',['../structMFontList.html#a340b5470e7e37ae8e54078c671d6feed',1,'MFontList']]],
  ['objects_1298',['objects',['../struct__M17NObjectArray.html#aced5345a3661b7ae8536c357cf908cbf',1,'_M17NObjectArray']]],
  ['open_1299',['open',['../structMFontDriver.html#a167911209a97207954553e6d1ad198c3',1,'MFontDriver']]],
  ['open_5fim_1300',['open_im',['../structMInputDriver.html#adab132de1505f5945e55f5a4f00805e4',1,'MInputDriver']]],
  ['orientation_5freversed_1301',['orientation_reversed',['../structMDrawControl.html#a1d5acd8d1a0da026c17c0c514ad6a303',1,'MDrawControl']]],
  ['otf_1302',['otf',['../structMFontCapability.html#aaebae8a8d6d54d48c265799f03dd369d',1,'MFontCapability']]],
  ['otf_5ftag_1303',['OTF_Tag',['../font_8h.html#ac5b9be741f31fdc336d7b915a42febb7',1,'font.h']]],
  ['outer_5fhmargin_1304',['outer_hmargin',['../structMFaceBoxProp.html#afef6a830345bf42aaf4c044f0bdaac20',1,'MFaceBoxProp']]],
  ['outer_5fvmargin_1305',['outer_vmargin',['../structMFaceBoxProp.html#ab5e4b4fe78b8f476fa6c94d678cf4465',1,'MFaceBoxProp']]]
];
