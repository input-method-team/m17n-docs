var searchData=
[
  ['x_1444',['x',['../structMInputContext.html#ad0ddea81107d27fcea91b833c6cd8c46',1,'MInputContext::x()'],['../structMDrawMetric.html#a16c503cd0a112cd513b9440c07e13aa0',1,'MDrawMetric::x()'],['../structMDrawGlyphInfo.html#a624fa8bbc40f462f3b6d84891539fcad',1,'MDrawGlyphInfo::x()'],['../structMDrawPoint.html#a6eddff33aba82b4a3c1a5f01dc10ed5b',1,'MDrawPoint::x()']]],
  ['x_5fadvance_1445',['x_advance',['../structMDrawGlyph.html#ac814c13052221a9e7195dfcdf5d4297f',1,'MDrawGlyph']]],
  ['x_5foff_1446',['x_off',['../structMDrawGlyph.html#a89b7f81aa679efbc999a1e5b1e37de60',1,'MDrawGlyph']]],
  ['x_5fppem_1447',['x_ppem',['../structMFLTFont.html#a4f5da0ed80dcff1a9518f8ea538dc805',1,'MFLTFont::x_ppem()'],['../structMRealizedFont.html#a8ddbbf430944fda77d393b0f53f739a6',1,'MRealizedFont::x_ppem()']]],
  ['xadv_1448',['xadv',['../structMFLTGlyph.html#adc2d36c54171be0b60910d450391ca90',1,'MFLTGlyph::xadv()'],['../structMFLTGlyphAdjustment.html#a15a8d5a2994dd192e2650fb9487760e9',1,'MFLTGlyphAdjustment::xadv()']]],
  ['xoff_1449',['xoff',['../structMFLTGlyph.html#ad1893a4e7b67bae84bf5f8969f0f74e2',1,'MFLTGlyph::xoff()'],['../structMFLTGlyphAdjustment.html#a8d57d0253fbf193fb4488a5e4bb2668d',1,'MFLTGlyphAdjustment::xoff()']]]
];
