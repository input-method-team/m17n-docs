var searchData=
[
  ['g_202',['g',['../structMGlyph.html#aff6661e8fba13ecb76ede3f015e26b0e',1,'MGlyph']]],
  ['get_5fglyph_5fid_203',['get_glyph_id',['../structMFLTFont.html#a187c3b748ab1b24576ffbc403e6aa36a',1,'MFLTFont']]],
  ['get_5fmetrics_204',['get_metrics',['../structMFLTFont.html#ac9264cefa810c378061758450ea6a29c',1,'MFLTFont']]],
  ['get_5fprop_205',['get_prop',['../structMDeviceDriver.html#a0bfb530b3ee5f42eeb96a104a3cd858f',1,'MDeviceDriver']]],
  ['glyph_5fanchor_206',['GLYPH_ANCHOR',['../internal-gui_8h.html#a59cdb484a4fbd668842b9d955a72ef38ae51bc23d74c488b92addd336707a5514',1,'internal-gui.h']]],
  ['glyph_5fbox_207',['GLYPH_BOX',['../internal-gui_8h.html#a59cdb484a4fbd668842b9d955a72ef38a12fb5810f9c2f13c4f24d47c8490157f',1,'internal-gui.h']]],
  ['glyph_5fcategory_208',['glyph_category',['../internal-gui_8h.html#a49d66330dc6f9b2174201aaa9dd82d9d',1,'internal-gui.h']]],
  ['glyph_5fcategory_5fformatter_209',['GLYPH_CATEGORY_FORMATTER',['../internal-gui_8h.html#a49d66330dc6f9b2174201aaa9dd82d9da94f0f38a9abefda08b21e474c871445d',1,'internal-gui.h']]],
  ['glyph_5fcategory_5fmodifier_210',['GLYPH_CATEGORY_MODIFIER',['../internal-gui_8h.html#a49d66330dc6f9b2174201aaa9dd82d9da2768de962f894d48913a7d64c98a8160',1,'internal-gui.h']]],
  ['glyph_5fcategory_5fnormal_211',['GLYPH_CATEGORY_NORMAL',['../internal-gui_8h.html#a49d66330dc6f9b2174201aaa9dd82d9da519b4880428a327dc8402655224387d1',1,'internal-gui.h']]],
  ['glyph_5fchar_212',['GLYPH_CHAR',['../internal-gui_8h.html#a59cdb484a4fbd668842b9d955a72ef38a550fd0816509136ef11474371786b58e',1,'internal-gui.h']]],
  ['glyph_5fcode_213',['glyph_code',['../structMDrawGlyph.html#aebd827c9ab5b6cfda97ff1d150594d8c',1,'MDrawGlyph']]],
  ['glyph_5findex_214',['GLYPH_INDEX',['../internal-gui_8h.html#a7a8c1576c870475d3f38f4d563236b8f',1,'internal-gui.h']]],
  ['glyph_5fpad_215',['GLYPH_PAD',['../internal-gui_8h.html#a59cdb484a4fbd668842b9d955a72ef38a8373dbc7605cf07da13d4d8a55728cf6',1,'internal-gui.h']]],
  ['glyph_5fsize_216',['glyph_size',['../structMFLTGlyphString.html#a3f4914499360ee6085733370a7827993',1,'MFLTGlyphString']]],
  ['glyph_5fspace_217',['GLYPH_SPACE',['../internal-gui_8h.html#a59cdb484a4fbd668842b9d955a72ef38a7bdaa8673462a465945d926cdf6851c6',1,'internal-gui.h']]],
  ['glyph_5ftype_218',['glyph_type',['../internal-gui_8h.html#a59cdb484a4fbd668842b9d955a72ef38',1,'internal-gui.h']]],
  ['glyph_5ftype_5fmax_219',['GLYPH_TYPE_MAX',['../internal-gui_8h.html#a59cdb484a4fbd668842b9d955a72ef38a051d3d121814ef528deae6715dd7b6d3',1,'internal-gui.h']]],
  ['glyphs_220',['glyphs',['../structMGlyphString.html#abe9b33d761cc547fa0f1e0db1c55cf47',1,'MGlyphString::glyphs()'],['../structMFLTGlyphString.html#af019589ab90ca6672fb31b65b7077af9',1,'MFLTGlyphString::glyphs()']]],
  ['gnu_20free_20documentation_20license_221',['GNU Free Documentation License',['../GFDL.html',1,'']]],
  ['gui_20api_222',['GUI API',['../group__m17nGUI.html',1,'']]]
];
