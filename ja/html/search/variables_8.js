var searchData=
[
  ['id_2079',['id',['../structMRealizedFont.html#acd3393e2976e9da49640d392c8c94939',1,'MRealizedFont']]],
  ['ignore_5fformatting_5fchar_2080',['ignore_formatting_char',['../structMDrawControl.html#a5552f972cdf5378dd22001e30ea76c6c',1,'MDrawControl']]],
  ['im_2081',['im',['../structMInputContext.html#a61a94b70f77af6c5c16111b13c17508d',1,'MInputContext']]],
  ['inc_2082',['inc',['../structMCharsetISO2022Table.html#aece951bf1e5fe804a6c76a55210c2b6f',1,'MCharsetISO2022Table::inc()'],['../structMFontPropertyTable.html#a9cb1e2438a53024af8c4e48114d26511',1,'MFontPropertyTable::inc()'],['../structMInputContextInfo.html#abcbcbd67b200ca9594ceb8b2c9a69cc9',1,'MInputContextInfo::inc()'],['../structMGlyphString.html#a888e5b4ef2c12c2b7315e7023fb01d69',1,'MGlyphString::inc()'],['../structM17NObjectRecord.html#a8c54d4a221ea955a8465df5df2840855',1,'M17NObjectRecord::inc()'],['../struct__M17NObjectArray.html#ab7de3a90a6b466164bae8fbabd7a3f76',1,'_M17NObjectArray::inc()']]],
  ['indent_2083',['indent',['../structMGlyphString.html#a5e8cc20912680dad042741d7a7737118',1,'MGlyphString']]],
  ['info_2084',['info',['../structMInputContext.html#a4da2ca307715dcbeca602aa5cfcb33c1',1,'MInputContext::info()'],['../structMRealizedFont.html#a5dcebdd6ffeb581cff59b0637087db0f',1,'MRealizedFont::info()'],['../structMRealizedFace.html#aef3aa4af466de05e39734f692c89dbf1',1,'MRealizedFace::info()'],['../structMInputMethod.html#a164148109aa2bba97e9308fb456919e8',1,'MInputMethod::info()']]],
  ['initial_5finvocation_2085',['initial_invocation',['../structMCodingInfoISO2022.html#a63a338d45583142ffde67f198800ee14',1,'MCodingInfoISO2022']]],
  ['inner_5fhmargin_2086',['inner_hmargin',['../structMFaceBoxProp.html#ac930be6305b09b92bfc59632cedb0888',1,'MFaceBoxProp']]],
  ['inner_5fvmargin_2087',['inner_vmargin',['../structMFaceBoxProp.html#a77d5aa80ca8407be0c4c5ac7d426bd8d',1,'MFaceBoxProp']]],
  ['input_5fstyle_2088',['input_style',['../structMInputXIMArgIC.html#a9f945020a38d416432b2a1b1e16f86ed',1,'MInputXIMArgIC']]],
  ['internal_2089',['internal',['../structMFLTGlyph.html#a0b2d21195c9e213070e3cd20b2432e8f',1,'MFLTGlyph::internal()'],['../structMFLTFont.html#a246902fe3b4b6a918a51daa05051a6ed',1,'MFLTFont::internal()']]],
  ['internal_5finfo_2090',['internal_info',['../structMConverter.html#a0316d9ad5f6ea12f166b4db59dc5ab3d',1,'MConverter']]],
  ['intersect_5fregion_2091',['intersect_region',['../structMDeviceDriver.html#a13de09d0c64302c4a0f7035ccded55d1',1,'MDeviceDriver']]],
  ['iterate_5fotf_5ffeature_2092',['iterate_otf_feature',['../structMFontDriver.html#ade0c493bf064837e389b472452ffe49b',1,'MFontDriver']]]
];
