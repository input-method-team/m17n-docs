var searchData=
[
  ['name_2367',['name',['../structMCharset.html#afa3cfd8b08592c4a70762687559310a9',1,'MCharset::name()'],['../struct__MInputMethodInfo.html#a77fc28cfed7b2b90a1db67ea90bab32d',1,'_MInputMethodInfo::name()'],['../struct__M17NObjectArray.html#a654e723f597a31650f1b703a5201e90d',1,'_M17NObjectArray::name()'],['../structMSymbolStruct.html#af613b7e1bf496d3121b4b794b3e09a9b',1,'MSymbolStruct::name()'],['../structMInputMethod.html#a45c41b1b26161fc5569fd2f32bc6ee58',1,'MInputMethod::name()']]],
  ['names_2368',['names',['../structMFontPropertyTable.html#a898a910ce8ae0ba2ec1e2c509121c19b',1,'MFontPropertyTable']]],
  ['nbytes_2369',['nbytes',['../structMConverter.html#a70d38d3bda2a382e037fbe72f7e46be1',1,'MConverter::nbytes()'],['../structMText.html#ab74890def46d45dd0f65c91d277a5d35',1,'MText::nbytes()']]],
  ['nchars_2370',['nchars',['../structMConverter.html#a6b7c87d3ce21346ff6543e7afadab693',1,'MConverter::nchars()'],['../structMText.html#a26dff32efa1331dd67337c656cbe4968',1,'MText::nchars()']]],
  ['next_2371',['next',['../structMRealizedFont.html#aafad4331ede13e8b37d1e3c9ec53c4c3',1,'MRealizedFont::next()'],['../structMSymbolStruct.html#ae5d3ac8b4616b8140aeb04f7825ffaf2',1,'MSymbolStruct::next()'],['../structMPlist.html#ae617edc4d0c6e4103242a7f0cf707603',1,'MPlist::next()'],['../struct__M17NObjectArray.html#ae0edd3066576b9afa609e37d002e5242',1,'_M17NObjectArray::next()'],['../structMGlyphString.html#a5f04de6f3b44883960224bd0ba8ab3f3',1,'MGlyphString::next()']]],
  ['next_5fto_2372',['next_to',['../structMDrawGlyphInfo.html#aafb3540a6d715bfcdf761ca9ba546689',1,'MDrawGlyphInfo']]],
  ['nfeatures_2373',['nfeatures',['../structMFontCapability.html#a775bb13c777187fd8f44bff843fb00ec',1,'MFontCapability']]],
  ['nfonts_2374',['nfonts',['../structMFontList.html#af2648678a84103ccb7316f3623f4017e',1,'MFontList']]],
  ['no_5fcode_5fgap_2375',['no_code_gap',['../structMCharset.html#af1af5e8ab3d44e03cb494a68d669bc00',1,'MCharset']]],
  ['non_5fascii_5flist_2376',['non_ascii_list',['../structMRealizedFace.html#a25baa883be19923d8d1c202d23b5682d',1,'MRealizedFace']]],
  ['nparents_2377',['nparents',['../structMCharset.html#a95a5a5af43bd14ff04dfbd1ce2798d93',1,'MCharset']]]
];
