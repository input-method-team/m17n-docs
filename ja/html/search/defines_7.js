var searchData=
[
  ['index_5fto_5fcode_5fpoint_2736',['INDEX_TO_CODE_POINT',['../charset_8h.html#a459cdfec5eec0dfa42ae33e8e5a48db2',1,'charset.h']]],
  ['init_5fglyph_2737',['INIT_GLYPH',['../internal-gui_8h.html#a92be04d3bdd9267d88da01f11b3d1686',1,'internal-gui.h']]],
  ['insert_5fglyph_2738',['INSERT_GLYPH',['../internal-gui_8h.html#a4fd720b12df9def51b353842069e37fe',1,'internal-gui.h']]],
  ['isalnum_2739',['ISALNUM',['../character_8h.html#a8c0056118f585fa052e0f76e3198db6a',1,'character.h']]],
  ['iso_5fmax_5fchars_2740',['ISO_MAX_CHARS',['../charset_8h.html#a76759223727a5efeb578267f7ab3d1e2',1,'charset.h']]],
  ['iso_5fmax_5fdimension_2741',['ISO_MAX_DIMENSION',['../charset_8h.html#aceaac08b430fd3a06094160c1e6c08a9',1,'charset.h']]],
  ['iso_5fmax_5ffinal_2742',['ISO_MAX_FINAL',['../charset_8h.html#a383dec58acb0079d2d924764997c589c',1,'charset.h']]],
  ['isupper_2743',['ISUPPER',['../character_8h.html#a2bfd4177f165a5968d95960d42578ab6',1,'character.h']]]
];
