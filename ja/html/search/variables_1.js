var searchData=
[
  ['back_1941',['back',['../structMFLTGlyphAdjustment.html#aee994941a0007062c69d1f81c9f12407',1,'MFLTGlyphAdjustment']]],
  ['background_1942',['background',['../structMFrame.html#a873f96f06328f30b4800c68c0184982b',1,'MFrame']]],
  ['base_5fface_5flist_1943',['base_face_list',['../structMRealizedFace.html#ac8953a26a544a33386b417cd7e8650f2',1,'MRealizedFace']]],
  ['baseline_5foffset_1944',['baseline_offset',['../structMRealizedFont.html#a0eaaa8306c56a9e7fff3cef7f3c070fd',1,'MRealizedFont']]],
  ['bc_5fcmds_1945',['bc_cmds',['../struct__MInputMethodInfo.html#aaba772122d02d2aede2c87fe4e361ee7',1,'_MInputMethodInfo']]],
  ['bc_5fvars_1946',['bc_vars',['../struct__MInputMethodInfo.html#a795cf6fed72e5c09afafaf5a3c58dcbf',1,'_MInputMethodInfo']]],
  ['bidi_5flevel_1947',['bidi_level',['../structMGlyph.html#a767272c59add40f6a6d86f4e4028f937',1,'MGlyph']]],
  ['bom_1948',['bom',['../structMCodingInfoUTF.html#a76136aaed031c21a9d8ea4ebc32bddf4',1,'MCodingInfoUTF']]],
  ['box_1949',['box',['../structMRealizedFace.html#abcb1be50f848a72efd622cb28f0a442f',1,'MRealizedFace']]]
];
