var indexSectionsWithContent =
{
  0: "_abcdefghiklmnoprstuvwxyはエコサシテデフプロ入文管表",
  1: "_m",
  2: "cdefilmpst",
  3: "dm",
  4: "abcdefghiklmnoprstuvwxy",
  5: "mo",
  6: "gm",
  7: "gm",
  8: "_acdefgimprstu",
  9: "fgmはエコシテデフプロ入文管表",
  10: "dgmtサ"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "groups",
  10: "pages"
};

var indexSectionLabels =
{
  0: "全て",
  1: "データ構造",
  2: "ファイル",
  3: "関数",
  4: "変数",
  5: "型定義",
  6: "列挙型",
  7: "列挙値",
  8: "マクロ定義",
  9: "グループ",
  10: "ページ"
};

