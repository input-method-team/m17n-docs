var searchData=
[
  ['tab_5fwidth_1390',['tab_width',['../structMDrawControl.html#a80fd394fb5a68f773c06badcff75a67f',1,'MDrawControl']]],
  ['tags_1391',['tags',['../structMFontCapability.html#a143d3928699bf0cc7d1c8586ecd875f0',1,'MFontCapability']]],
  ['text_5fascent_1392',['text_ascent',['../structMGlyphString.html#a32266bfe470619b8240edab5429c3675',1,'MGlyphString']]],
  ['text_5fdescent_1393',['text_descent',['../structMGlyphString.html#a7b11a817f37d238dd43ea65564f36992',1,'MGlyphString']]],
  ['textprop_2ec_1394',['textprop.c',['../textprop_8c.html',1,'']]],
  ['textprop_2eh_1395',['textprop.h',['../textprop_8h.html',1,'']]],
  ['tick_1396',['tick',['../struct__MInputMethodInfo.html#a4fae115d700b92f743ee13a6474762ed',1,'_MInputMethodInfo::tick()'],['../structMInputContextInfo.html#a2b7a2c732a30a8083e1426216ff141ac',1,'MInputContextInfo::tick()'],['../structMFrame.html#a4c0e76c10fde6b2f6c99b234e5313370',1,'MFrame::tick()'],['../structMGlyphString.html#aceb8840e83120cc4fb6a8f9f0611585b',1,'MGlyphString::tick()']]],
  ['time_1397',['time',['../structMDatabaseInfo.html#ae24789ceac297929fbf7ba42f7ca5c9f',1,'MDatabaseInfo']]],
  ['title_1398',['title',['../struct__MInputMethodInfo.html#ac57013bd73f9c9f7ed23beefbd36b33e',1,'_MInputMethodInfo']]],
  ['to_1399',['to',['../structMFLTGlyph.html#ac53dc0c1cbf143c355b844f82906b702',1,'MFLTGlyph::to()'],['../structMGlyphString.html#a4c260a92e22efec4d964c6acc5145ca6',1,'MGlyphString::to()'],['../structMDrawGlyph.html#a41c5fd578f7167e8d9bc31ae9df0dccf',1,'MDrawGlyph::to()'],['../structMDrawGlyphInfo.html#a959a29a28118d7db13a375b8163a7f01',1,'MDrawGlyphInfo::to()']]],
  ['tolower_1400',['TOLOWER',['../character_8h.html#ad4771ad2e5dd6a87c83ecf879b7985de',1,'character.h']]],
  ['top_1401',['top',['../structMGlyphString.html#ade100f4c29591fc9c086226844a26286',1,'MGlyphString']]],
  ['toupper_1402',['TOUPPER',['../character_8h.html#ab54417c7c018570043920af66ba7457f',1,'character.h']]],
  ['try_5fotf_1403',['try_otf',['../structMFontDriver.html#af66fc2164b7da149302691a0c4ff040f',1,'MFontDriver']]],
  ['tutorial_20for_20writing_20the_20m17n_20database_1404',['Tutorial for writing the m17n database',['../m17nDBTutorial.html',1,'']]],
  ['two_5fdimensional_1405',['two_dimensional',['../structMDrawControl.html#aa8078b02cc0922ee8c67b9633b68ec14',1,'MDrawControl']]],
  ['type_1406',['type',['../structMFaceHLineProp.html#acedd85c2e75b96980f7d39d1a4614c7d',1,'MFaceHLineProp::type()'],['../structMFont.html#a51bddada1813f60c5c6f18b622650211',1,'MFont::type()'],['../structMGlyph.html#a584da491bb68f1c945359bd1b6886343',1,'MGlyph::type()']]]
];
