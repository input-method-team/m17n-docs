var searchData=
[
  ['y_1432',['y',['../structMInputContext.html#a1bce06fc197b83fbe61ebe0b0d366c94',1,'MInputContext::y()'],['../structMDrawMetric.html#a0792ce7cbaf96fbad00f4f5cd895f2f2',1,'MDrawMetric::y()'],['../structMDrawGlyphInfo.html#a6ce5994024cbaa72e4b0f6e4f6a8749f',1,'MDrawGlyphInfo::y()'],['../structMDrawPoint.html#a2a647f6b3d642dd64766cfb86ed9f852',1,'MDrawPoint::y()']]],
  ['y_5fadvance_1433',['y_advance',['../structMDrawGlyph.html#a0ea0c7bb474929dd6094af188b72ad13',1,'MDrawGlyph']]],
  ['y_5foff_1434',['y_off',['../structMDrawGlyph.html#a0aa9c8644380aaa7572efc60b020a0d4',1,'MDrawGlyph']]],
  ['y_5fppem_1435',['y_ppem',['../structMFLTFont.html#ad645f26ac88930e973a637163c4cfae3',1,'MFLTFont::y_ppem()'],['../structMRealizedFont.html#a100bc511c6495ef76b0a6085a837f34e',1,'MRealizedFont::y_ppem()']]],
  ['yadv_1436',['yadv',['../structMFLTGlyph.html#a4f6bdf7dcd6eb716dfbd04d3bebe0715',1,'MFLTGlyph::yadv()'],['../structMFLTGlyphAdjustment.html#af0883a32e2ecd73cd823c0af21fa12d0',1,'MFLTGlyphAdjustment::yadv()']]],
  ['yoff_1437',['yoff',['../structMFLTGlyph.html#a4ab4bb4a600a8eb211d2b29b7118f9fa',1,'MFLTGlyph::yoff()'],['../structMFLTGlyphAdjustment.html#a8a4a85f7ca77f747499e53398b5cd7be',1,'MFLTGlyphAdjustment::yoff()']]]
];
